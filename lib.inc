section .text    

%define SYS_EXIT 60
%define NULL_TERMINATOR 0
%define ASCII_NUMBERS_OFFSET 0x30
%define ASCII_NUMBERS_MAX 0x39
%define DECIMAL_RADIX 10
%define TAB 0x9

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; counter
    .string_length_loop:
        cmp byte [rdi+rax], 0
        je .string_length_end
        inc rax
        jmp .string_length_loop
    .string_length_end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov     rsi, rdi        ; save string beginning pointer
    call    string_length   ; get string length in rax
    mov     rdx, rax        ; copy length to [size for syscall]
    mov     rdi, 1          ; [syscall stdout]
    mov     rax, 1          ; [syscall sys_write]
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp                 ; reserve 1 byte in the stack for the char
; since one char has one byte size, we can reserve only one byte in the stack
; thus, we must put in the stack only one lower byte of rdi, which is dil (the smallest rdi segment register)
    mov [rsp], dil          ; write char to stack by rsp
    mov rsi, rsp            ; char address
    mov rdx, 1              ; output length
    mov rdi, 1              ; stdout descriptor
    mov rax, 1              ; 'write' syscall number
    syscall
    inc rsp                 ; free space reserved in the stack
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, '\n'
    jmp print_char

; writes number to the stack as chars
convert_number_to_string:
    pop rcx                               ; save return address
    dec rsp                               ; return rsp to previous position
    mov byte [rsp], NULL_TERMINATOR       ; push null-terminator

    mov rax, rdi
    mov rdi, 10             ; radix
    .convert_number_loop:
        xor rdx, rdx                   ; here remainder will be stored
        div rdi
        add rdx, ASCII_NUMBERS_OFFSET  ; numbers have 0x30 offset in the ASCII table
        dec rsp
        mov [rsp], dl                  ; push char to the stack (lower byte of the rdx register)
        cmp rax, 0
        jne .convert_number_loop       ; if ratio is not a 0 then repeat division
; chars of the number is on top of the stack now
    push rcx                           ; load return address
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
; since we will need to clear the stack later, we have to save its state before any operations
    push r12                ; first we need to save current value of the callee-save register we are going to use
    mov r12, rsp            ; save current state of the stack
    call convert_number_to_string
    mov rdi, rsp            ; beginning of the string for print_string call
    call print_string
    mov rsp, r12            ; restore state of the stack (simply clear it)
    pop r12                 ; restore r12 state
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    push r12                ; kind of similar to print_uint
    mov r12, rsp
    mov rax, rdi
    cmp rax, 0
    jge .print_positive_int
    .print_negative_int:
        neg rdi
        call convert_number_to_string
        dec rsp
        mov byte [rsp], '-'
        jmp .print_int_end
    .print_positive_int:
        call convert_number_to_string
    .print_int_end:
        mov rdi, rsp
        call print_string
        mov rsp, r12
        pop r12
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r10, r10                ; let's store char index in r10
    .string_equals_iterate:
        mov al, byte [rdi+r10]          ; read first char
        cmp al, byte [rsi+r10]          ; compare to the second char
        jne .string_equals_return_0     ; if not equals return 0
        cmp al, NULL_TERMINATOR         ; check whether char is null-terminator
        je .string_equals_return_1      ; if it is, return 1
        inc r10                         ; else increment current char index
        jmp .string_equals_iterate      ; and iterate
    .string_equals_return_0:
        mov rax, 0
        ret
    .string_equals_return_1:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax    ; syscall read
    xor rdi, rdi    ; from stream 0 (stdin)
    mov rdx, 1      ; read one char (size count)
    dec rsp         ; reserve byte from the stack
    mov rsi, rsp    ; buffer address
    syscall
    cmp rax, 1      ; syscall returns how many bytes were read
    je .read_char_success
    .read_char_eof:
        inc rsp
        ret
    .read_char_success:
        mov al, [rsp]
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi    ; buffer address
    mov r13, rsi    ; buffer size
    xor r14, r14    ; char index


    .read_word_iterate:                 ; main loop
            call read_char
            cmp al, NULL_TERMINATOR
            je .read_word_success
    .read_word_check_spaces:
        cmp al, ' '
        je .read_word_handle_space
        cmp al, TAB
        je .read_word_handle_space
        cmp al, '\n'
        je .read_word_handle_space
        jmp .read_word_write_char
    .read_word_handle_space:
        cmp r14, 0
        ja .read_word_success
        jmp .read_word_iterate
    .read_word_write_char:
        mov [r12+r14], al
        inc r14
        cmp r14, r13
        jbe .read_word_iterate
    .read_word_overflow:
        xor rax, rax
        xor rdx, rdx
        jmp .read_word_end
    .read_word_success:
        mov byte [r12+r14], NULL_TERMINATOR
        mov rax, r12
        mov rdx, r14
    .read_word_end:
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx                        ; char index
    mov r10, DECIMAL_RADIX
    xor rsi, rsi                        ; temporary storage for char
    .parse_uint_iterate:
        mov sil, [rdi+rcx]              ; read char
        cmp sil, ASCII_NUMBERS_OFFSET
        jb .parse_uint_end
        cmp sil, ASCII_NUMBERS_MAX
        ja .parse_uint_end
        sub sil, ASCII_NUMBERS_OFFSET
        mul r10                         ; shift rax (multiply by 10)
        add rax, rsi
        inc rcx
        jmp .parse_uint_iterate
    .parse_uint_end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    .parse_negative_int:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .parse_int_failed
        neg rax
        inc rdx
        ret
    .parse_int_failed:
        xor rax, rax
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx                    ; index of char to copy
    .string_copy_iterate:
        cmp rcx, rdx
        je .string_copy_overflow
        mov al, byte [rdi+rcx]
        mov byte[rsi+rcx], al
        inc rcx
        cmp al, NULL_TERMINATOR     ; check if null-terminator
        je .string_copy_end
        jmp .string_copy_iterate
    .string_copy_overflow:
        xor rax, rax
        ret
    .string_copy_end:
        mov rax, rcx
        ret
